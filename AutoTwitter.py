#!/usr/bin/python
import sys
import os.path
import urllib.request as urlrq
import certifi
import datetime
import collections
from twython import Twython
from twython import TwythonError
from bs4 import BeautifulSoup

#Bot acc info
CONSUMER_KEY = ''
CONSUMER_SECRET = ''
ACCESS_KEY = ''
ACCESS_SECRET = ''

debug = False

#Additional Twitter variables
CHARACTER_LIMIT = 280
URL_CHARACTER_COUNT = 23
url = 'https://www.whitehouse.gov/briefing-room/presidential-actions/'

#Other variables
script_dir = os.path.dirname(__file__)
saved_page = os.path.join(script_dir, 'page_content.html')
last_entry_file = os.path.join(script_dir, 'last_entry.txt')
error_log = os.path.join(script_dir, 'error_log.txt')
api = Twython(CONSUMER_KEY,CONSUMER_SECRET,ACCESS_KEY,ACCESS_SECRET)
current_time = datetime.datetime.now()
c = collections.Counter('Statement from')
isLong = False
lines = []
myTweets = []

#Get last entry if it exists
if os.path.exists(last_entry_file):
    with open(last_entry_file, 'r', encoding="utf-8") as f:
        previous_link = f.read().replace('\n','')
else:
    previous_link = ""

#Get page content
page = urlrq.urlopen(url, cafile=certifi.where())
page_content = page.read()

#Get previously saved page content
if os.path.exists(saved_page):
    old_page = open(saved_page, 'r', encoding="utf-8")
    old_page_content = old_page.read()
else:
    print("No previous page to compare to. Data is being recorded, please run again later")
    with open(saved_page, 'wb') as fid:
        fid.write(page_content)
    sys.exit(0)

#If page content is different, check for new entries
if old_page_content != page_content:
    #Update saved page content to reflect change
    with open(saved_page, 'wb') as fid:
        fid.write(page_content)

    #Variables
    data = []
    links = []
    with open(saved_page, 'r', encoding="utf-8") as fid:
        soup = BeautifulSoup(fid, "lxml")
    found = False

    #Check all entries with 'article' tag
    for entry in soup.find_all('article'):
        name = ""
        for string in entry.h2.a.stripped_strings:
            name += string + " "
        name = name[:-1]
        link = entry.h2.a.get('href')
        if (debug):
            print("Article Found!")
            print("Name: %s" % name)
            print("Link: %s" % link)
        if (link == previous_link):
            found = True
            break
        if(len(name) > (CHARACTER_LIMIT - URL_CHARACTER_COUNT - 1)):#The extra 1 is to put a space between the words and the link
            tempString = name
            extra = (CHARACTER_LIMIT - URL_CHARACTER_COUNT - 1 - 3) - len(name)#The 3 is used to append: '...'
            tempString = tempString[:extra]
            tempString += "..."
            data.append(tempString)
        else:
            data.append(name)
        links.append(link)

    #If previous link was not found on this page, quit the program and log the error/time
    if not found:
        with open (last_entry_file, 'w') as f:
            f.write(links[0])
        errorString = current_time.strftime('[%Y-%m-%d %H:%M:%S]')
        errorString += " PREVIOUS LINK WAS NOT FOUND. ACTION TERMINATED."

        if os.path.exists(error_log):
            with open (error_log, 'a') as f:
                f.write("\n" + errorString)
        else:
            with open (error_log, 'w') as f:
                f.write(errorString)
        sys.exit()

    #If previous link was the latest, no new entries exist.
    if len(data) < 1:
        print("No new articles!")
        sys.exit()

    #For each new entry: append entry name and link into one string: my_tweet
    for i, val in enumerate(data):
        my_tweet = data[i]
        my_tweet += " "
        my_tweet += links[i]
        myTweets.append(my_tweet)

    #Tweet each string in myTweets, starting from the end of the array
    #Attempt to tweet the string
    for i in range((len(myTweets) -1), -1, -1):
        try:
            if (debug):
                print("Attempting to tweet: %s" % myTweets[i])
            api.update_status(status=myTweets[i])
        #If an exception is thrown, log the error code and string
        except TwythonError as e:
            errorString = current_time.strftime('[%x]')
            errorString += " Returned Error Code: "
            errorString += str(e.error_code)
            errorString += " while trying to tweet: "
            errorString += myTweets[i]
            errorString += "\n"

            if os.path.exists(error_log):
                with open (last_entry_file, 'a') as f:
                    f.write(errorString)
            else:
                with open (last_entry_file, 'w') as f:
                    f.write(errorString)
            pass
    if (debug):
        print("Finished tweeting. Cleaning things up now...")

    #Update last entry
    with open (last_entry_file, 'w') as f:
        f.write(links[0])
else:
        print("Site did not change. Quitting...")
        sys.exit()
