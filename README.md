# Executive Orders - Python Twitter Bot

Checks the official White House website for updates in the "Presidential Actions" section and tweets out any new links. Written in python (using Twython and BeautifulSoup). Must be run once beforehand for initialization of local files.
[Twitter](https://twitter.com/BidenExecOrders)